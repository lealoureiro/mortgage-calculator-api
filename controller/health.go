package controller

import (
	"encoding/json"
	"net/http"

	"github.com/lealoureiro/mortgage-calculator-api/model"
	log "github.com/sirupsen/logrus"
)

// Health : get application health
func Health(w http.ResponseWriter, r *http.Request) {

	// swagger:route GET /health API info
	// ---
	// description: get application health
	// responses:
	//   200: health

	log.WithFields(log.Fields{"remote_addr": r.RemoteAddr}).Info("Providing application health")

	info := model.Health{Status: "OK"}

	j, _ := json.Marshal(info)

	w.Header().Set("Content-Type", "application/json")

	w.Write([]byte(j))
}
