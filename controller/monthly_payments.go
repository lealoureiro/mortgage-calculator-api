package controller

import (
	"encoding/json"
	"net/http"

	"github.com/lealoureiro/mortgage-calculator-api/model"
	"github.com/lealoureiro/mortgage-calculator-api/monthlypayments"
	"github.com/lealoureiro/mortgage-calculator-api/utils"
	log "github.com/sirupsen/logrus"
)

// MonthlyPayments : REST resource to calculate Mortgage Monthly Payments
func MonthlyPayments(w http.ResponseWriter, r *http.Request) {

	// swagger:route POST /monthly-payments API monthlyPaymentsResquest
	// ---
	// description: Calculate Linear Mortgage Monthly Payments
	// responses:
	//   200: monthlyPaymentsResponse
	//   400: badRequestResponse

	log.WithFields(log.Fields{"remote_addr": r.RemoteAddr}).Info("Providing Linear Mortgage monthly payments")

	if !isMonthlyPaymentsRequestValid(w, r) {
		return
	}

	var rb model.MonthlyPaymentsRequest

	d := json.NewDecoder(r.Body)
	err := d.Decode(&rb)

	if err != nil {
		utils.RespondHTTPError(400, err.Error(), w)
		return
	}

	valid, err2 := monthlypayments.ValidateInputData(rb)

	if !valid {
		log.WithFields(log.Fields{"error": err2}).Error("Failed to validate input data")
		utils.RespondHTTPError(400, err2, w)
		return
	}

	log.WithFields(log.Fields{
		"initial_principal":         rb.InitialPrincipal,
		"market_value":              rb.MarketValue.AsFloat(),
		"months":                    rb.Months,
		"automatic_interest_update": rb.AutomaticInterestUpdate,
		"repayments":                rb.Repayments,
	}).Info("Calculating monthly payments")

	mps := monthlypayments.CalculateLinearMonthlyPayments(rb)

	mpsJSON, err3 := json.Marshal(mps)

	if err3 != nil {
		log.WithFields(log.Fields{"error": err3}).Error("Error serializing the response when calculating monthly payments")
		utils.RespondHTTPError(500, err3.Error(), w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(mpsJSON)

}

func isMonthlyPaymentsRequestValid(w http.ResponseWriter, r *http.Request) bool {

	if !utils.IsContentTypeJSON(r) {
		utils.RespondHTTPError(415, "Unsupported Media Type", w)
		return false
	}

	return true
}
