FROM golang:1.23.2-alpine3.20 AS builder

LABEL maintainer="Leandro Loureiro <leandroloureiro@pm.me>"

ARG APP_VERSION=dev

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY controller controller
COPY model model
COPY monthlypayments monthlypayments
COPY utils utils
COPY config config
COPY mortgage-calculator-api.go .

RUN CGO_ENABLED=0 \
    GOOS=linux \
    go build -a -ldflags "-X 'github.com/lealoureiro/mortgage-calculator-api/config.Version=${APP_VERSION}'" \
    -installsuffix cgo \
    -o /go/bin/mortgage-calculator-api .

FROM alpine:3.20

RUN apk --no-cache add curl

WORKDIR /

COPY --from=builder /go/bin/mortgage-calculator-api .

EXPOSE 5000

HEALTHCHECK --start-period=1s --interval=60s --timeout=1s --retries=1 \
    CMD curl --silent --fail http://localhost:5000/health || exit 1

ENTRYPOINT ["/mortgage-calculator-api"]