//		Mortgage Calculator API
//
//	    Schemes: https
//	    BasePath: /
//	    Version: 1.0.0
//	    License: MIT http://opensource.org/licenses/MIT
//	    Contact: Leandro Loureiro <leandroloureiro@pm.me>
//	    Host: secret-journey-21988.herokuapp.com
//
//	    Consumes:
//	    - application/json
//
//	    Produces:
//	    - application/json
//
// swagger:meta
package main

import (
	"net/http"
	"os"

	"github.com/lealoureiro/mortgage-calculator-api/config"
	"github.com/lealoureiro/mortgage-calculator-api/controller"
	log "github.com/sirupsen/logrus"
)

// CORSEnabledRouter : Router to add CORS headers
type CORSEnabledRouter struct {
	r *http.ServeMux
}

func main() {

	port := os.Getenv("PORT")

	if port == "" {
		port = "5000"
	}

	log.SetFormatter(&log.TextFormatter{
		FullTimestamp: true,
	})

	log.WithFields(log.Fields{"version": config.Version}).Info("Starting Mortgage Calculator API")
	log.WithFields(log.Fields{"port": port}).Info("Receiving requests")

	r := http.NewServeMux()
	r.HandleFunc("/info", controller.ShowInfo)
	r.HandleFunc("/health", controller.Health)
	r.HandleFunc("/monthly-payments", controller.MonthlyPayments)

	http.Handle("/", &CORSEnabledRouter{r})
	err := http.ListenAndServe(":"+port, nil)

	if err != nil {
		log.WithFields(log.Fields{"error": err.Error()}).Error("Failed to start HTTP Server.")
	}

}

// function to add CORS headers
func (s *CORSEnabledRouter) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}

	if r.Method == "OPTIONS" {
		return
	}

	s.r.ServeHTTP(w, r)

}
